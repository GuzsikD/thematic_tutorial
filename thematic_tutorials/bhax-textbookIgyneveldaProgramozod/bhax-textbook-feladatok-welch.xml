<chapter xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xi="http://www.w3.org/2001/XInclude" version="5.0" xml:lang="hu">
    <info>
        <title>Helló, Welch!</title>
        <keywordset>
            <keyword/>
        </keywordset>
    </info>
    <section>
        <title>Első osztályom</title>
        <para>
            Valósítsd meg C++-ban és Java-ban az módosított polártranszformációs algoritmust! A matek háttér 
            teljesen irreleváns, csak annyiban érdekes, hogy az algoritmus egy számítása során két normálist
            számol ki, az egyiket elspájzolod és egy további logikai taggal az osztályban jelzed, hogy van vagy
            nincs eltéve kiszámolt szám.
        </para>
        <para>
            Az algoritmus készítéséhez segítségül szolgált a <link xlink:href="https://www.tankonyvtar.hu/hu/tartalom/tkt/javat-tanitok-javat/ch01.html">Javát tanítok</link> honlapján
            található útmutatás és az <link xlink:href="https://sourceforge.net/projects/udprog/">UDPROG közösség</link> repója.
        </para>
        <para>
            Polártranszformációs algoritmus C++:
        </para>
        <para>
            <programlisting language = "c++"><![CDATA[#include <iostream>
#include <cstdlib>	
#include <cmath>	
#include <ctime>	

using namespace std;

class Random
{

	public:

		Random();

		~Random(){} 

		double get();
	private:

		bool exist;
		double value;

};]]></programlisting>
            Létrehozzuk a <prompt>Random</prompt> osztályt, a publikus részben az osztály konstruktora, 
            destruktora és a random számokat lekérő függvény fog helyet kapni, míg a privát részben azt fogjuk tárolni, 
            hogy létezik-e korábban kiszámolt másik random, illetve ennek mi az értéke.
        </para>
        <para>
            <programlisting language = "c++"><![CDATA[Random::Random() 
{
	exist = false;
	srand (time(NULL));
};]]></programlisting>
            A konstruktorban inicializáljuk a randomszám generáló <function>srand</function> függvényt.  
        </para>
        <para>
            <programlisting language = "c++"><![CDATA[double Random::get() 
{
	if (!exist)
	{
		double u1, u2, v1, v2, w;

		do
		{
			u1 = rand () / (RAND_MAX + 1.0);
			u2 = rand () / (RAND_MAX + 1.0);
			v1 = 2 * u1 - 1;
			v2 = 2 * u2 - 1;
			w = v1 * v1 + v2 * v2;
		    }
		while (w > 1);

		double r = sqrt ((-2 * log (w)) / w);

		value = r * v2; 
		exist = !exist;

		return r * v1;
	}
	
	else
	{
		exist = !exist;
		return value;
	}
};]]></programlisting>
            A lekérő függvény. Amennyiben nincs eltárolva random számunk, akkor létrehozunk két normálist. Az elsőt ki fogjuk íratni, a másikat eltároljuk.
            Ha van eltárolva szám, akkor csak annyit csinálunk, hogy azt kiíratjuk.
        </para>
        <para>
            <programlisting language = "c++"><![CDATA[int main() 
{

	Random rnd;

	for (int i = 0; i < 2; ++i) cout << rnd.get() << endl;

}]]></programlisting>
            A <function>main()</function> függvényben már nem sok tennivalónk van, csak meghívjuk a random osztály lehívó függvényét.
        </para>
        <para>
            <programlisting><![CDATA[$ g++ random_class.cpp -o random
$ ./random
2.46919
0.688235
]]></programlisting>
        </para>
        <para>
            Polártranszformációs algoritmus JAVA:
        </para>
        <para>
            <programlisting><![CDATA[public class PolárTranszF {
    
    boolean létezik_tárolt = false;
    double tárolt;
    
    public PolárTranszF() {
        
        létezik_tárolt = false;
        
    }]]></programlisting>
            A publikus PolárTranszF osztályunkban deklaráljuk a logikai tagot, ami azt figyeli, hogy van-e korábban eltárolt 
            random, illetve ennek az értékét.
        </para>
        <para>
            <programlisting><![CDATA[public double matek_rész() {
        
        if(!létezik_tárolt) {
            
            double u1, u2, v1, v2, w;
            do {
                u1 = Math.random();
                u2 = Math.random();
                
                v1 = 2*u1 - 1;
                v2 = 2*u2 - 1;
                
                w = v1*v1 + v2*v2;
                
            } while(w > 1);
            
            double r = Math.sqrt((-2*Math.log(w))/w);
            
            tárolt = r*v2;
            létezik_tárolt = !létezik_tárolt;
            
            return r*v1;
            
        } else {
            létezik_tárolt = !létezik_tárolt;
            return tárolt;
        }
    }]]></programlisting>
            Ha nincs korábban eltárolt random értékünk, akkor meghívjuk a misztikus random-generáló 
            matematikai eljárást. Amennyiben volt eltárolva, akkor cask az értékét adjuk vissza és a logikai 
            tagot átállítjuk.
        </para>
        <para>
            <programlisting><![CDATA[    public static void main(String[] args) {
        
        PolárTranszF g = new PolárTranszF();
        
        for(int i=0; i<2; ++i)
            System.out.println(g.matek_rész());
        
    }
    
}]]></programlisting>
            Majd legvégül kétszer meghívjuk a misztikus matematikai eljárást. Első hívásnál ki fogja nekünk írni, a frissen 
            kiszámolt számok egyikét, második hívásnál pedig az első alkalommal kiszámolt, majd elmentett értéket adja vissza.    
        </para>
        <para>
            <programlisting><![CDATA[$ javac PolárTranszF.java
$ java PolárTranszF
0.678582973383662
1.6936497301003002
]]></programlisting>
        </para>
        <para>
            Ha "fellapozzuk" a Java Development Kit <prompt>src.zip</prompt>-ben , azon belül a <prompt>/java/util/</prompt>
            elérési útvonalon található <filename>Random.java</filename>
            fájlt, akkor tapasztalhatjuk, hogy nem állunk túl messze a valóságtól. Ugyanis az 583. sortól megtalálhatjuk
            a JAVA saját randomszám generátorát a <function>nextGaussian</function> fantázianévre hallgató függvényt, ami szinte azonos
            a fentebb bemutatott c++-os és JAVA-s példákkal.
        </para>        
        
        <para>
            Megoldás forrása:<link xlink:href="https://www.tankonyvtar.hu/hu/tartalom/tkt/javat-tanitok-javat/ch01.html">https://www.tankonyvtar.hu/hu/tartalom/tkt/javat-tanitok-javat/ch01.html/</link>                
        </para>
                      
    </section>        


    <section>
        <title>LZW</title>
        <para>
            Valósítsd meg C-ben az LZW algoritmus fa-építését!
        </para>
        <para>
            Az  Lempel–Ziv–Welch (LZW) algoritmust az  1980-as évek közepén, Abraham Lempel és Jacob Ziv már létező algoritmusát 
            továbbfejlesztve, Terry Welch publikálta. Az algoritmus a UNIX alapú rendszerek fájltömörítő segédprogramja által
            terjed el leginkább, továbbá GIF kiterjesztésű képek és PDF fájlok veszteségmentes tömörítéséhez is használják. 
            Az USA-ban 2003-ban, a világ többi részén 2004-ben az algoritmus szabadalma lejárt, ezért azóta alkalmazása a háttérbe szorult.
        </para>
        <para>
            A bináris fát úgy építjük föl, hogy a bemenetre érkező nullákat és egyeseket olvassuk. 
            Ha olyan egységet olvasunk be, ami már korábban volt, akkor olvassuk tovább. 
            Az így kapott új egységet fogjuk a fa gyökerétől kezdve "lelépkedni". 
            Ha az adott egység ábrázolva van, visszaugrunk a gyökérbe és olvassuk tovább az inputot.
        </para>
        <figure>
            <title>Binfa felépítése:</title>
            <mediaobject>
                <imageobject>
                    <imagedata fileref="img/Inordertree.png" scale="50" />
                </imageobject>
                <textobject>
                    <phrase>Binfa felépítése:</phrase>
                </textobject>
            </mediaobject>
        </figure>
        <para>
            A program készítéséhez <link xlink:href="https://progpater.blog.hu/2011/02/19/gyonyor_a_tomor">Programozó Páternoszter</link>-en található 
            útmutatót hívtam segítségül.
        </para>
        <para>
            A kód bemutatása lépésről lépésre:
            
            <programlisting language = 'c'><![CDATA[#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

typedef struct binfa
{
  int ertek;
  struct binfa *bal_nulla;
  struct binfa *jobb_egy;

} BINFA, *BINFA_PTR;]]></programlisting>
            Itt definiáljuk a binfa struktúrát.
        </para>
        <para>     
            <programlisting language = 'c'><![CDATA[BINFA_PTR
uj_elem ()
{
  BINFA_PTR p;

  if ((p = (BINFA_PTR) malloc (sizeof (BINFA))) == NULL)
    {
      perror ("memoria");
      exit (EXIT_FAILURE);
    }
  return p;
}]]></programlisting>
            Az új elem létrehozásakor memóriát szabadítunk majd fel, hiba esetén viszont kilépünk a programból.
        </para>
        <para>     
            <programlisting language = 'c'><![CDATA[extern void kiir (BINFA_PTR elem);
extern void szabadit (BINFA_PTR elem);]]></programlisting>
            Deklaráljuk, de még nem definiáljuk a fa kiírásához és a lefoglalt memória felszabadításához használt függvényeket.
            
        </para>
        <para>     
            <programlisting language = 'c'><![CDATA[  int
  main (int argc, char **argv)
  {
    char b;

    BINFA_PTR gyoker = uj_elem ();
    gyoker->ertek = '/';
    BINFA_PTR fa = gyoker;

    while (read (0, (void *) &b, 1))
      {
        write (1, &b, 1);
        if (b == '0')
  	{
  	  if (fa->bal_nulla == NULL)
  	    {
  	      fa->bal_nulla = uj_elem ();
  	      fa->bal_nulla->ertek = 0;
  	      fa->bal_nulla->bal_nulla = fa->bal_nulla->jobb_egy = NULL;
  	      fa = gyoker;
  	    }
  	  else
  	    {
  	      fa = fa->bal_nulla;
  	    }
  	}
        else
  	{
  	  if (fa->jobb_egy == NULL)
  	    {
  	      fa->jobb_egy = uj_elem ();
  	      fa->jobb_egy->ertek = 1;
  	      fa->jobb_egy->bal_nulla = fa->jobb_egy->jobb_egy = NULL;
  	      fa = gyoker;
  	    }
  	  else
  	    {
  	      fa = fa->jobb_egy;
  	    }
  	}
      }

    printf ("\n");
    kiir (gyoker);
    extern int max_melyseg;
    printf ("melyseg=%d\n", max_melyseg);
    szabadit (gyoker);
  }]]></programlisting>
            Először létrehozzuk a gyökeret és ráállítjuk a fa pointert. Olvassuk az inputon érkező 0-kat és 1-eseket.          
        </para>
        <para>
            Ha az aktuális node-nak nincs meg az a gyermeke, amelyik tipusú elemet épp olvassuk (0 vagy 1) akkor az uj_elem függvény
            létrehoz egyet neki és értékül megkapja a beolvasott elemet. Az új elem gyermekeit pedig null pointerekre állítja és vissza ugrik a pointer a gyökérre.
        </para>
        
        <para>     
            <programlisting language = 'c'><![CDATA[static int melyseg = 0;
  int max_melyseg = 0;

  void
  kiir (BINFA_PTR elem)
  {
    if (elem != NULL)
      {
        ++melyseg;
        if (melyseg > max_melyseg)
  	max_melyseg = melyseg;
        kiir (elem->jobb_egy);
        for (int i = 0; i < melyseg; ++i)
  	printf ("---");
        printf ("%c(%d)\n", elem->ertek < 2 ? '0' + elem->ertek : elem->ertek,
  	      melyseg);
        kiir (elem->bal_nulla);
        --melyseg;
      }
  }]]></programlisting>
            A <function>kiír</function> függvénnyel jelenítjük meg magát a fát a parancssorban. Mivel 90 fokkal el 
            van forgatva az eredmény balra és fentről lefelé írjuk ki az ágakat ezért inorder bejárás esetén először 
            a jobb oldali ágat, majd a gyökeret, majd végül a bal oldali ágat rajzoltatjuk ki. Közben számljuk a fa mélységét is.
        </para>
        <para>     
            <programlisting language = 'c'><![CDATA[  void
  szabadit (BINFA_PTR elem)
  {
    if (elem != NULL)
      {
        szabadit (elem->jobb_egy);
        szabadit (elem->bal_nulla);
        free (elem);
      }
  }]]></programlisting>
            Rekurzívan hívjuk a függvényt, amivel felszabadítjuk a korábban lefoglalt elemeket.
        </para>
        <para>
            A programot fordítva és futtatva tetszőleges inputtal:
        </para>
        
            <mediaobject>
                <imageobject>
                    <imagedata fileref="img/binfa.png" scale="50" />
                </imageobject>
                <textobject>
                    <phrase>Binfa felépítése:</phrase>
                </textobject>
            </mediaobject>
        
        <para>
            Megoldás forrása:<link xlink:href="https://progpater.blog.hu/2011/02/19/gyonyor_a_tomor">https://progpater.blog.hu/2011/02/19/gyonyor_a_tomor/</link>              
        </para>
    </section>        
        
    <section>
        <title>Fabejárás</title>
        <para>
            Járd be az előző (inorder bejárású) fát pre- és posztorder is!
        </para>
        <para>
            Az előző LZW bináris fa feladatban a fát Inorder módon jártuk be. 
            Azaz a bal oldali részfát dolgozuk fel, majd a gyökeret és végül a jobb oldali részfát.            
        </para>
        <figure>
            <title>Emlékeztetőül az Inorder bejárásra:</title>
            <mediaobject>
                <imageobject>
                    <imagedata fileref="img/Inordertree.png" scale="50" />
                </imageobject>
                <textobject>
                    <phrase>Emlékeztetőül az Inorder bejárásra:</phrase>
                </textobject>
            </mediaobject>
        </figure>
        <para>
            A forráskódon belül, ezt a <function>kiír</function> függvénynél láthattuk.
            <programlisting language = 'c'><![CDATA[static int melyseg = 0;
  int max_melyseg = 0;

  void
  kiir (BINFA_PTR elem)
  {
    if (elem != NULL)
      {
        ++melyseg;
        if (melyseg > max_melyseg)
  	max_melyseg = melyseg;
        kiir (elem->jobb_egy);
        for (int i = 0; i < melyseg; ++i)
  	printf ("---");
        printf ("%c(%d)\n", elem->ertek < 2 ? '0' + elem->ertek : elem->ertek,
  	      melyseg);
        kiir (elem->bal_nulla);
        --melyseg;
      }
  }]]></programlisting>
        </para>
        <para>
            A program a következő fát rajzolta ki nekünk:         
        </para>
        <mediaobject>
                <imageobject>
                    <imagedata fileref="img/binfa.png" scale="50" />
                </imageobject>
        </mediaobject>
        <para>
            A preorder bejárás a következőképpen fog alakulni. Először mindig a gyökeret dolgozzuk fel,
            majd a bal oldali részfát (ott is először a gyökeret) és végül a jobb oldali részfát (ott is a gyökeret).
        </para>
        
        <para>
            A forráskódon a következő apró módosítást hajtjuk végre:
            <programlisting language = 'c'><![CDATA[void
kiir (BINFA_PTR elem)
{
  if (elem != NULL)
    {
      ++melyseg;
      if (melyseg > max_melyseg)
	       max_melyseg = melyseg;
       
      kiir (elem->jobb_egy);
      kiir (elem->bal_nulla);
      for (int i = 0; i < melyseg; ++i)
	       printf ("---");
      printf ("%c(%d)\n", elem->ertek < 2 ? '0' + elem->ertek : elem->ertek, melyseg);

      --melyseg;
    }
}]]></programlisting>
        </para>
        <para>
            A program a következő fát rajzolta ki nekünk:            
        </para>
        <mediaobject>
                <imageobject>
                    <imagedata fileref="img/binfapre.png" scale="50" />
                </imageobject>
        </mediaobject>
        <para>
            A postorder bejárásnál először mindig a bal oldali részfát (ott is először a bal részfát) 
            majd a jobb oldali részfát (ott is a jobb részfát) dolgozzuk fel. A legvégére marad a gyökér.
        </para>
        <figure>
            <title>Példa Postorder bejárásra:</title>
            <mediaobject>
                <imageobject>
                    <imagedata fileref="img/Postordertree.png" scale="50" />
                </imageobject>
                <textobject>
                    <phrase>Példa Postorder bejárásra:</phrase>
                </textobject>
            </mediaobject>
        </figure>
        <para>
            A forráskódon a következő apró módosítást hajtjuk végre:
            <programlisting language = 'c'><![CDATA[void
kiir (BINFA_PTR elem)
{
  if (elem != NULL)
    {
      ++melyseg;
      if (melyseg > max_melyseg)
	max_melyseg = melyseg;

      for (int i = 0; i < melyseg; ++i)
	    printf ("---");
      printf ("%c(%d)\n", elem->ertek < 2 ? '0' + elem->ertek : elem->ertek, melyseg);
      kiir (elem->jobb_egy);
      kiir (elem->bal_nulla);
      --melyseg;
    }
}]]></programlisting>
        </para>
        <para>
            A program a következő fát rajzolta ki nekünk:         
        </para>
        <mediaobject>
                <imageobject>
                    <imagedata fileref="img/binfapost.png" scale="50" />
                </imageobject>
        </mediaobject>
        
    </section>        
                        
    <section>
        <title>Tag a gyökér</title>
        <para>
            Az LZW algoritmust ültesd át egy C++ osztályba, legyen egy Tree és egy beágyazott Node
            osztálya. A gyökér csomópont legyen kompozícióban a fával!
        </para>
        <para>
            A bináris fa építését az újonnan létrehozott <prompt>LZWTree</prompt> osztályban szeretnénk megvalósítani.
            Ide kellene még beágyaznunk a fa csomópontjának ( <prompt>Node</prompt> ) a jellemzését is. 
            Azért fogjuk beágyazni, mert egyelőre semmilyen különleges szerepet nem kap.
            Szimplán csak a fa részeként tekintünk rá.
            <programlisting language = 'c++'><![CDATA[#include <iostream>

class LZWTree
{
public:
    LZWTree (): fa(&gyoker){}

    ~LZWTree ()
    {
        szabadit (gyoker.egyesGyermek ());
        szabadit (gyoker.nullasGyermek ());
    }

    void operator<<(char b)
    {
        if (b == '0')
        {

            if (!fa->nullasGyermek ())
            {
                Node *uj = new Node ('0');
                fa->ujNullasGyermek (uj);
                fa = &gyoker;
            }
            else
            {
                fa = fa->nullasGyermek ();
            }
        }
        else
        {
            if (!fa->egyesGyermek ())
            {
                Node *uj = new Node ('1');
                fa->ujEgyesGyermek (uj);
                fa = &gyoker;
            }
            else
            {
                fa = fa->egyesGyermek ();
            }
        }
    }
    void kiir (void)
    {
        melyseg = 0;
        kiir (&gyoker);
    }
    void szabadit (void)
    {
        szabadit (gyoker.jobbEgy);
        szabadit (gyoker.balNulla);
    }]]></programlisting>   
            A fa konstruktora és destruktora után az <prompt>LZWTree</prompt> osztályon belül definiáljuk a balra eltoló bitshift operátor 
            túlterhelését. Ez segíteni fog az inputról érkező karaktereket "beletolni" a <prompt>LZWTree</prompt> objektumba.
            Így fog felépülni a fa.
        </para>
        <para>            
            <programlisting language = 'c++'><![CDATA[private:

    class Node
    {
    public:
        Node (char b = '/'):betu (b), balNulla (0), jobbEgy (0) {};
        ~Node () {};
        Node *nullasGyermek () {
            return balNulla;
        }
        Node *egyesGyermek ()
        {
            return jobbEgy;
        }
        void ujNullasGyermek (Node * gy)
        {
            balNulla = gy;
        }
        void ujEgyesGyermek (Node * gy)
        {
            jobbEgy = gy;
        }
        
    private:
        friend class LZWTree;
        char betu;
        Node *balNulla;
        Node *jobbEgy;
        Node (const Node &);
        Node & operator=(const Node &);
    };]]></programlisting>
            Amennyiben paraméter nélküli a <prompt>Node</prompt> konstruktor, akkor az alapértelmezett '/'-jellel fogja azt létrehozni.
            Egyébként a meghívó karakter kerül a betű helyére. A bal és jobb gyermekekre mutató mutatókat nulára állítjuk.
            Az aktuális <prompt>Node</prompt> mindig meg tudja mondani, hogy mi a bal illetve jobb gyermeke. Meg is 
            mondhatjuk neki, hogy melyik csomópont legyen ezentúl az új gyermek. Barátjaként deklaráljuk az <prompt>LZWTree</prompt> 
            osztályt, hogy az dolgozhasson a csomópontokkal.
        </para>
        <para>           
            <programlisting language = 'c++'><![CDATA[    Node gyoker;
    Node *fa;
    int melyseg;

    LZWTree (const LZWTree &);
    LZWTree & operator=(const LZWTree &);

    void kiir (Node* elem)
    {
        if (elem != NULL)
        {
            ++melyseg;
            kiir (elem->jobbEgy);

            for (int i = 0; i < melyseg; ++i)
                std::cout << "---";
            std::cout << elem->betu << "(" << melyseg - 1 << ")" << std::endl;
            kiir (elem->balNulla);
            --melyseg;
        }
    }
    void szabadit (Node * elem)
    {
        if (elem != NULL)
        {
            szabadit (elem->jobbEgy);
            szabadit (elem->balNulla);
            delete elem;
        }
    }

};]]></programlisting>
            Mindig az aktuális csomópontra mutatunk. A <function>kiír</function> függvénnyel jelenítjük meg magát a fát.
            Felszabadítjuk a két gyermeket, nehogy elfolyjon a memóriánk.
        </para>        
        <para>    
            <programlisting language = 'c++'><![CDATA[int
main ()
{
    char b;
    LZWTree binFa;

    while (std::cin >> b)
    {
        binFa << b;
    }

    binFa.kiir ();
    binFa.szabadit ();

    return 0;
}]]></programlisting>
            Bitenként olvassuk a bemenetet, de a fát már karakterenként építjük fel.
        </para>
        <para>
            Fordítás és futtatás után: 
            <programlisting><![CDATA[$ g++ tree.cpp -o tree
$ ./tree < teszt.txt
------------1(3)
---------1(2)
------1(1)
---------0(2)
------------0(3)
---------------0(4)
---/(0)
---------1(2)
------0(1)
---------0(2)
]]></programlisting>            
        </para>
                
        <para>
            Megoldás forrása:<link xlink:href="https://progpater.blog.hu/2011/04/01/imadni_fogjak_a_c_t_egy_emberkent_tiszta_szivbol_2">https://progpater.blog.hu/2011/04/01/imadni_fogjak_a_c_t_egy_emberkent_tiszta_szivbol_2</link>               
        </para>
        <para>
            Továbbá:<link xlink:href="https://progpater.blog.hu/2011/04/14/egyutt_tamadjuk_meg">https://progpater.blog.hu/2011/04/14/egyutt_tamadjuk_meg</link>               
        </para>
    </section>        
                
    <section>
        <title>Mutató a gyökér</title>
        <para>
            Írd át az előző forrást, hogy a gyökér csomópont ne kompozícióban, csak aggregációban legyen a 
            fával!
        </para>
        <para>
            Eddig a <prompt>LZWTree</prompt>-ben a fa gyökere mindig egy objektum volt. A <prompt>fa</prompt> mutatót kezdetben 
            ráállítottuk és a gyökér referencia értékét adta vissza. Mostantól a gyökér is és a fa is egy-egy mutató lesz.
            Tehát nem a referenciát adjuk át a fa mutatónak, hanem magát a gyökér mutatót.
        </para>
        <para>    
            <programlisting language = 'c++'><![CDATA[class LZWTree
{
public:
    LZWTree () 
    {
        gyoker  = new Node();
        fa = gyoker;
    }

    ~LZWTree ()
    {
         szabadit (gyoker->egyesGyermek ());
         szabadit (gyoker->nullasGyermek ());
         delete gyoker;
    }]]></programlisting>
            A konstruktorban a gyökeret új csomópontra mutató mutatóként hozzuk létre, a destruktorban
            pedig ugyanígy szabadítjuk fel. Az eddig a gyökér előtt állő referenciajeleket mindenhonnan kitörölgetjük.            
        </para>
        <para>    
            <programlisting language = 'c++'><![CDATA[    Node *gyoker;
    Node *fa;
    int melyseg;]]></programlisting>
            A csomópontban a védett tagok között is objektumként szerepelt eddig, ott is át kell írni pointerré.           
        </para>
        <para>
            Láásuk hogy fordul-e és fut-e a program a módosítások után?  
            <programlisting><![CDATA[$ g++ pointer.cpp -o pointer
$ ./pointer < teszt.txt
------------1(3)
---------1(2)
------1(1)
---------0(2)
------------0(3)
---------------0(4)
---/(0)
---------1(2)
------0(1)
---------0(2)
]]></programlisting>                   
        </para>
        
    </section>                     

    <section>
        <title>Mozgató szemantika</title>
        <para>
            Írj az előző programhoz mozgató konstruktort és értékadást, a mozgató konstruktor legyen a mozgató
            értékadásra alapozva!
        </para>
       <para>
            A feladatot az <link xlink:href="https://sourceforge.net/projects/udprog/">UDPROG közösség</link> repója segítségével készítettem el.
        </para>
        <para>
        Mozgató konstruktor helyett itt egy másoló konstruktor:
        <programlisting language = 'c++'><![CDATA[    LZWTree& operator= (LZWTree& copy) //másoló értékadás
    {
        szabadit(gyoker->egyesGyermek ()); //régi értéket törlöm
        szabadit(gyoker->nullasGyermek ());
        
        bejar(gyoker,copy.gyoker); //rekurzívan bejárom a fákat és átmásolom az értékeket

        fa = gyoker; //mindkét fában visszaugrok a gyökérhez 
        copy.fa = copy.gyoker;
    }]]></programlisting>
        </para>
        <para>
            <programlisting language = 'c++'><![CDATA[        void bejar (Node * masolat, Node * eredeti) //rekurzív famásolás, másoló értékadáshoz
        {

            if (eredeti != NULL) //ha létezik a másolandó fa
            {

                if ( !eredeti->nullasGyermek() ) //ha nem létezik az eredeti nullasgyermeke
                {
                    masolat->ujNullasGyermek(NULL);
                }
                else //ha létezik az eredeti nullásgyermeke
                {
                //létrehozni a masolat nullasgyermeket és meghívni újra a bejart
                Node* uj = new Node ('0');
                masolat->ujNullasGyermek (uj);
                bejar(masolat->nullasGyermek(), eredeti->nullasGyermek());
                }

                if ( !eredeti->egyesGyermek() ) //ha nem létezik az eredeti egyesgyermeke
                {
                    masolat->ujEgyesGyermek(NULL);
                }
                else //ha létezik az eredeti egyesgyermeke
                {
                //létrehozni a masolat egyesgyermeket és meghívni újra a bejart
                Node *uj = new Node ('1');
                    masolat->ujEgyesGyermek (uj);
                bejar(masolat->egyesGyermek(), eredeti->egyesGyermek());
                }
            }
            else //ha nem létezik a másolandó fa
            {
                masolat = NULL;
            }
        }]]></programlisting>
        </para>
    </section>                     
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
</chapter>                
