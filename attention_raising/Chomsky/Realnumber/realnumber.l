%{
#include <stdio.h>
int valossz_db = 0;
%}
digit	[0-9]
%%
{digit}*(\.{digit}+)?	{++valossz_db; 
    printf("[valosszam=%s %f]", yytext, atof(yytext));}
%%
int
main ()
{
 yylex ();
 printf("Valós számok darabszáma %d\n", valossz_db);
 return 0;
}

