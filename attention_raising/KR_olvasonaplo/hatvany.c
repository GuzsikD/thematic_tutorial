#include <stdio.h>


/*Hatványozó függvény tesztelése*/
/*x n­dik hatványra emelése; n >0*/
power (int x,int n) 
{
	int i, p;
	p = 1;
	for (i = 1; i <= n; ++i)
		p = p * x;
	return (p);
}

int
main ()
{
	int i;
	for (i = 0; i < 10; ++i)
		printf ("%d %d %d\n", i, power ( 2,i), power (­-3,i));
	return 0;
}