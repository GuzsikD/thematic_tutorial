#include <stdio.h>

/* Fahrenheit-fok-Celsius-fok táblázat kiírása F = 0, 20,
..., 300 Fahrenheit-fokra; a program lebegőpontos
változata */
main ( ) {
   float fahr, celsius;
   int also, felso, lepes;
   also = 0;      /* a táblázat alsó határa */
   felso = 300;  /* a táblázat felső határa */
   lepes = 20;   /* a táblázat lépésköze */
   fahr = also;
   while (fahr <= felso) {
      celsius = (5.0/9.0) * (fahr-32.0);
      printf("%3.0f %6.1f\n", fahr, celsius);
      fahr = fahr + lepes;
   }
}
