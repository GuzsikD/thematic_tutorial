#include <stdio.h>

/* Megszámlálja a bemeneten érkező karaktereket*/
int
main()
{
	long nc;
	nc = 0;
	while (getchar () != EOF)
		++nc;
	printf("%ld\n", nc);
	return 0;
}