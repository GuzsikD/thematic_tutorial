#include <stdio.h>
#include <curses.h>
#include <unistd.h>

int
main ( void )
{
    WINDOW *ablak; //mutató az aktuális konzolablakra
    ablak = initscr (); //amíg a program fut, ezt az ablakot használja

    int x = 0;
    int y = 0; //labda kezdőpozíciók

    int xnov = 1;
    int ynov = 1; //a léptetés mértéke

    int mx;
    int my; //az ablak mérete

    for (;;)
    {
        getmaxyx ( ablak, my , mx ); //az ablak aktuális méretét hívja be

        mvprintw ( y, x, "O" ); //a labda kirajzoltatása

        refresh (); //valós időben frissítjük az ablakot
        usleep ( 100000 ); //mikrosec-ben mérve, altatjuk a programot bizonyos időre
        //a labda sebességét is ez határozza meg
        clear(); //letakarítjuk a konzolt, hogy csak az aktuális labda jelenjen meg

        x = x + xnov;
        y = y + ynov; //mozgatjuk a labdát a koordináták növelésével

        if ( x>=mx-1 ) // ha elértük a konzol jobb oldalát, visszafordulunk
        {
            xnov = xnov * -1;
        }
        if ( x<=0 ) // ha elértük a konzol bal oldalát, visszafordulunk
        {
            xnov = xnov * -1;
        }
        if ( y<=0 ) // ha elértük a konzol tetejét, visszafordulunk
        {
            ynov = ynov * -1;
        }
        if ( y>=my-1 ) // ha elértük a konzol alját, visszafordulunk
        {
            ynov = ynov * -1;
        }
    }
    return 0;
}

