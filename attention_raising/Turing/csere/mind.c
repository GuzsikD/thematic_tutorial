#include <stdio.h>
#define val1 1986 
#define val2 2033

signed int temp;

void swap(int*,int*, int);

int main(){
    
    int a = val1, b = val2;
    
    printf("Csere összeadással/kivonással:\n");
    
    printf("Kezdeti értékek:\t a=%d b=%d\n", a, b);
    swap(&a,&b,0);
    printf("Csere után:\t\t a=%d b=%d\n\n", a, b);
    
    printf("Csere XOR-ral (kizáró vagy bitenként):\n");
    
    printf("Kezdeti értékek:\t a=%d b=%d\n", a, b);
    swap(&a,&b,1);
    printf("Csere után:\t\t a=%d b=%d\n\n", a, b);
    
    printf("Csere segédváltozóval:\n");
    
    printf("Kezdeti értékek:\t a=%d b=%d\n", a, b);
    swap(&a,&b,2);
    printf("Csere után:\t\t a=%d b=%d\n\n", a, b);
    
    return 0;
}

void swap(int* a,int* b, int swapMode)
{
    switch(swapMode)
    {
        case 0: 
            *b = *b - *a;
            *a = *a + *b;
            *b = *a - *b;
            return;
        case 1:
            *b = *a ^ *b;
            *a = *a ^ *b;
            *b = *a ^ *b;
            return;
            
        default:
            temp = *a;
            *a = *b;
            *b = temp;
            return;
    }
}

