#include <stdio.h>
#define val1 a 
#define val2 b

signed char temp;

void swap(char*,char*, char);

int main(){
    
    char a = val1, b = val2;
    
    printf("Csere összeadással/kivonással:\n");
    
    printf("Kezdeti értékek:\t a=%c b=%c\n", a, b);
    swap(&a,&b,0);
    printf("Csere után:\t\t a=%c b=%c\n\n", a, b);
    
    printf("Csere XOR-ral (kizáró vagy bitenként):\n");
    
    printf("Kezdeti értékek:\t a=%d b=%d\n", a, b);
    swap(&a,&b,1);
    printf("Csere után:\t\t a=%d b=%d\n\n", a, b);
    
    printf("Csere segédváltozóval:\n");
    
    printf("Kezdeti értékek:\t a=%d b=%d\n", a, b);
    swap(&a,&b,2);
    printf("Csere után:\t\t a=%d b=%d\n\n", a, b);
    
    return 0;
}

void swap(char* a,char* b, char swapMode)
{
    switch(swapMode)
    {
        case 0: 
            *b = *b - *a;
            *a = *a + *b;
            *b = *a - *b;
            return;
        case 1:
            *b = *a ^ *b;
            *a = *a ^ *b;
            *b = *a ^ *b;
            return;
            
        default:
            temp = *a;
            *a = *b;
            *b = temp;
            return;
    }
}

