#include <iostream>
#include <fstream>


class LZWBinaryTree
{
public:
   
    LZWBinaryTree ()
    {
        currentNode = root;
    }
    ~LZWBinaryTree ()
    {
        
        free (root);
 
    }

    LZWBinaryTree (LZWBinaryTree&& other)
    {
        
        root = nullptr;
        *this = std::move(other);
    }
    
    LZWBinaryTree& operator=(LZWBinaryTree&& other)
    {
        std::swap(root, other.root);
	std::swap(currentNode, other.currentNode);
        return *this;
    }
    
    
    void operator<< (char b)
    {
       
        if (b == '0')
        {
            if (!currentNode->getLeftChild ())
            {
                Node *uj = new Node ('0');
                currentNode->newLeftChild (uj);
               
                currentNode = root;
            }
            else			
            {
                currentNode = currentNode->getLeftChild ();
            }
        }
        else
        {
            if (!currentNode->getRightChild ())
            {
                Node *uj = new Node ('1');
                currentNode->newRightChild (uj);
                currentNode = root;
            }
            else
            {
                currentNode = currentNode->getRightChild ();
            }
        }
    }
 
    void print (void)
    {
       
        depth = 0;
        print (root, std::cout);
    }
   
    int getDepth (void);

    friend std::ostream & operator<< (std::ostream & os, LZWBinaryTree & bf)
    {
        bf.print (os);
        return os;
    }
    void print (std::ostream & os)
    {
        depth = 0;
        print (root, os);
    }

private:
    class Node
    {
    public:
      
        Node (char b = '/'):value (b), leftChild (0), rightChild (0)
        {
        };
        ~Node ()
        {
        };
       
        Node *getLeftChild () const
        {
            return leftChild;
        }
       
        Node *getRightChild () const
        {
            return rightChild;
        }
      
        void newLeftChild (Node * gy)
        {
            leftChild = gy;
        }
       
        void newRightChild (Node * gy)
        {
            rightChild = gy;
        }
       
        char getValue () const
        {
            return value;
        }

    private:
        char value;
       
        Node *leftChild;
        Node *rightChild;
        
        Node (const Node &);
        Node & operator= (const Node &);
    };

    
    Node *currentNode;
    
    int depth;
   

    
    LZWBinaryTree (const LZWBinaryTree &);
    LZWBinaryTree & operator= (const LZWBinaryTree &);

    
    void print (Node * n, std::ostream & os)
    {
        
        if (n != NULL)
        {
            ++depth;
            print (n->getLeftChild (), os);
           
            for (int i = 0; i < depth; ++i)
                os << "---";
            os << n->getValue () << "(" << depth << ")" << std::endl;
            print (n->getRightChild (), os);
            --depth;
        }
    }
    void free (Node * n)
    {
        
        if (n != NULL)
        {
            free (n->getLeftChild ());
            free (n->getRightChild ());
            
            delete n;
        }
    }

protected:
    Node* root = new Node();
    int maxDepth;

    void getDepthRec (Node * n);

};


int LZWBinaryTree::getDepth (void)
{
    depth = maxDepth = 0;
    getDepthRec (root);
    return maxDepth;
}

void LZWBinaryTree::getDepthRec (Node * n)
{
    if (n != NULL)
    {
        ++depth;
        if (depth > maxDepth)
            maxDepth = depth;
        getDepthRec (n->getRightChild ());
        getDepthRec (n->getLeftChild ());
        --depth;
    }
}


void usage (void)
{
    std::cout << "Usage: lzwtree in_file" << std::endl;
}

int main (int argc, char *argv[])
{
    
    if (argc != 2)
    {
        usage ();
        return -1;
    }

    
    char *inFile = *++argv;

    std::fstream beFile (inFile, std::ios_base::in);

    if (!beFile)
    {
        std::cout << inFile << "Nem létezik a bemeneti fájl!" << std::endl;
        usage ();
        return -3;
    }


    char b;		
    LZWBinaryTree binFa;	


    while (beFile.read ((char *) &b, sizeof (char)))
    {

        if(b == '0')
        {
            binFa << b;
        }
        else if(b == '1')
        {
            binFa << b;
           
        }

    }
    std::cout << "Eredeti fa:\n\n";
    std::cout << binFa;
    

    std::cout << "depth = " << binFa.getDepth () << std::endl;

    LZWBinaryTree binFa2 = std::move(binFa);
     
    
    std::cout << "\nEredeti fa mozgatás után:\n";
    std::cout << binFa;
    
    
    
    std::cout << "depth = " << binFa.getDepth () << std::endl;

    std::cout << "\nMozgatással létrejött fa:\n\n";
    std::cout << binFa2;
    

    std::cout << "depth = " << binFa2.getDepth () << std::endl;
    
    beFile.close ();
    std::cout << &binFa;
    std::cout << "\n";
    std::cout << &binFa2;
    std::cout << "\n";
    return 0;
}
