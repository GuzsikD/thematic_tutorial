#include <iostream>
#include <cstdlib>
#include <cmath>
#include <ctime>


//Random osztály
class Random {

	public:

		Random(); //konstruktor

		~Random(){} //destruktor

		double get(); //random lekérés

	private:

		bool exist; //van-e korábbi érték
		double value; //random értéke

};


Random::Random() { //a konstruktor kifejtése
	exist = false;
	std::srand (std::time(NULL)); //random inicializálás
};


double Random::get() { //random lekérő függvény kifejtése
	if (!exist)
{
		double u1, u2, v1, v2, w;

		do{
			u1 = std::rand () / (RAND_MAX + 1.0); //innentől jön az algoritmus
			u2 = std::rand () / (RAND_MAX + 1.0);
			v1 = 2 * u1 - 1;
			v2 = 2 * u2 - 1;
			w = v1 * v1 + v2 * v2;
		    }
		while (w > 1);

		double r = std::sqrt ((-2 * std::log (w)) / w);

		value = r * v2;
		exist = !exist;

		return r * v1; //idáig tart az algoritmus
	}

	else
	{
		exist = !exist; //ha van korábbi random érték, akkor azt adja vissza
		return value;
	}
};

int main()
{

	Random rnd;

	for (int i = 0; i < 10; ++i) std::cout << rnd.get() << std::endl; //10 random szám generálása

}
